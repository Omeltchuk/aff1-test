<?php


namespace App\Contracts\Repositories;


use App\Models\Message;
use Illuminate\Support\Collection;

interface MessagesRepositoryInterface
{
	public function __construct(Message $model);

	public function getWithSchedule() : Collection;
}