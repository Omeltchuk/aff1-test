<?php


namespace App\Contracts\Repositories;


use App\Models\Customer;
use Illuminate\Support\Collection;

interface CustomersRepositoryInterface
{
	public function __construct(Customer $model);

	public function groupedByTimezoneOffset() : Collection;
}