<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ScheduledMail extends Mailable
{
    use Queueable, SerializesModels;

    private $text;

	/**
	 * Create a new message instance.
	 *
	 * @param string $text
	 */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.scheduled', ['text' => $this->text]);
    }
}
