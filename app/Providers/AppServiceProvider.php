<?php

namespace App\Providers;

use App\Contracts\Repositories\CustomersRepositoryInterface;
use App\Contracts\Repositories\MessagesRepositoryInterface;
use App\Repositories\CustomersRepository;
use App\Repositories\MessagesRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind(CustomersRepositoryInterface::class, CustomersRepository::class);

	    $this->app->bind(MessagesRepositoryInterface::class, MessagesRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
