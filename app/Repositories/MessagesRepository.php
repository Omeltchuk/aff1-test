<?php


namespace App\Repositories;


use App\Contracts\Repositories\MessagesRepositoryInterface;
use App\Models\Message;
use Illuminate\Support\Collection;

class MessagesRepository implements MessagesRepositoryInterface
{
	protected $model;

	public function __construct(Message $model)
	{
		$this->model = $model;
	}

	public function getWithSchedule() : Collection
	{
		return $this->model->with('schedule')->get();
	}
}