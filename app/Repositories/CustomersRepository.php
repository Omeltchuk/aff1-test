<?php


namespace App\Repositories;


use App\Contracts\Repositories\CustomersRepositoryInterface;
use App\Models\Customer;
use Illuminate\Support\Collection;

class CustomersRepository implements CustomersRepositoryInterface
{
	protected $model;

	public function __construct(Customer $model)
	{
		$this->model = $model;
	}

	public function groupedByTimezoneOffset() : Collection
	{
		return $this->model->select('email', 'timezone_offset')->get()->groupBy('timezone_offset');
	}
}