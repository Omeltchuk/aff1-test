<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\CustomersRepositoryInterface;
use App\Contracts\Repositories\MessagesRepositoryInterface;
use App\Mail\ScheduledMail;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class SendMails extends Command
{
	/**
	 * @var Collection
	 */
	protected $customersGrouped;

	/**
	 ** @var Collection
	 */
	protected $messages;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mails';

	/**
	 * Create a new command instance.
	 *
	 * @param CustomersRepositoryInterface $customersRepository
	 * @param MessagesRepositoryInterface $messagesRepository
	 */
    public function __construct(
    	CustomersRepositoryInterface $customersRepository,
		MessagesRepositoryInterface $messagesRepository
    )
    {
    	$this->customersGrouped = $customersRepository->groupedByTimezoneOffset();
    	$this->messages = $messagesRepository->getWithSchedule();

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    foreach ($this->customersGrouped as $customerOffset => $offsetGroup) {

		    $currentOffsetMessages = $this->getMessagesByOffset($customerOffset);

		    $emails = $offsetGroup->pluck('email');

		    $this->sendMailsToQueue($currentOffsetMessages, $emails);
    	}
    }

	protected function getMessagesByOffset($customerOffset) : Collection
	{
		$customerTime = now()->tz('UTC')->subHours($customerOffset)->format('H:i');

		return $this->messages->filter(function ($message) use ($customerTime) {
			return $message->schedule->pluck('send_at')->contains($customerTime);
		});
    }

	protected function sendMailsToQueue(Collection $messages, Collection $emails) : void
	{
		foreach ($messages as $message) {
			Mail::to($emails)->queue(new ScheduledMail($message->text));
		}
    }
}
