<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Schedule;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Schedule::class, function (Faker $faker) {
    return [
	    'send_at' => $faker->time('H:i'),
	    'message_id' => rand(1,50)
    ];
});
