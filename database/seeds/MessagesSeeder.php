<?php

use Illuminate\Database\Seeder;
use App\Models\Message;
use App\Models\Schedule;

class MessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    factory(Message::class, 100)->create()->each(function ($message) {
	        $message->schedule()->saveMany(factory(Schedule::class, 10)->make());
        });
    }
}
